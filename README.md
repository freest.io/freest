# Freest

Freest is a decentralized version control hosting service for public and open source code repositories.

The Freest platform will consist of two products:
- A source code hosting platform and set of core collaboration tools
- An app store where client-facing apps may be advertised and accessed